package com.hyh.gamehelper

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.blankj.utilcode.util.RomUtils
import com.hyh.gamehelper.ui.theme.GameHelperTheme
import com.tencent.mmkv.MMKV

class MainActivity : ComponentActivity() {
    private lateinit var mainViewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel = defaultViewModelProviderFactory.create(MainViewModel::class.java)
        mainViewModel.init(this)
        setContent {
            GameHelperTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Greeting(
                        "基于无障碍模式开发的闪现APP辅助操作,目前只有每日抢小鸡，下面输入小鸡在兑换列表里面的位置，例如：第一个就是0，第二个就是1。" +
                                "设置成功之后，如果无障碍模式没开启，需要在无障碍模式设置界面手动开启,如果没有自动跳转到无障碍模式设置界面，请手动打开手机的设置界面，自己设置一下",
                        mainViewModel = mainViewModel
                    )
                }
            }
        }
    }


}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Greeting(name: String, modifier: Modifier = Modifier, mainViewModel: MainViewModel) {
    Column {
        Text(
            text = "Hello $name!",
            modifier = modifier
        )
        val textValue = remember {
            mutableStateOf("")
        }
        TextField(
            value = textValue.value,
            onValueChange = { textValue.value = it },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            label = { Text(text = "小鸡在兑换列表的位置") },
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
        )
        Button(onClick = {
            mainViewModel.saveFlashChickPosition(textValue.value.toIntOrNull() ?: 0)
            mainViewModel.startAction()
        }) {
            Text(text = "保存")
        }
        Text(
            text = "一键签到领取积分需要先打开无障碍模式",
            modifier = modifier
        )
        Button(onClick = {
            mainViewModel.saveStartFlashCircleTag()
            mainViewModel.openFlashApp(true)
        }) {
            Text(text = "闪现APP圈子一键签到领取积分")
        }
    }

}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    GameHelperTheme {
        Greeting("闪现APP辅助操作", mainViewModel = MainViewModel())
    }
}