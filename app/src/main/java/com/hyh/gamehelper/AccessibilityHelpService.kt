package com.hyh.gamehelper

import android.accessibilityservice.AccessibilityService
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import androidx.compose.ui.geometry.Rect
import com.blankj.utilcode.util.TimeUtils
import com.tencent.mmkv.MMKV
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.Calendar
import kotlin.math.min

/**
 * 无障碍模式开发
 */
class AccessibilityHelpService : AccessibilityService() {
    private val spilt = 15 * 1000
    private val startAppSpilt = 60 * 1000
    private var lastStartAppTime = 0L
    private var chickExchangeTime = 0L
    private val opSpilt = 1 * 1000L
    private var lastTabClickTime = 0L
    private var lastOpAddTime = 0L
    private var acquirePointsTime = 0L

    /**
     * 1-选中圈子，2-点击打卡按钮，3-领取积分并关闭
     */
    private var pointsStep = 1

    /**
     * 1-
     */
    private var chickStep = 1

    /**
     * 闪现一下的包名
     */
    val toPackageName = "com.tencent.gamecommunity"
    val appName = "闪现一下"
    private var circleOpIndex = 0
    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        Log.e("AccessibilityHelpService", "无障碍服务 onAccessibilityEvent:${event}")
        if (startAction()) {
            startFlashApp()
            when (chickStep) {
                1 -> selectFlashAppMine()
                2 -> clickPointsEvent()
                3 -> chickNodeClick()
                4 -> confirmExchangeChick()
            }
        } else if (rootInActiveWindow?.packageName == toPackageName) {
            val needAutoSign =
                MMKV.defaultMMKV(MMKV.MULTI_PROCESS_MODE, null).getBoolean(MMKVKeys.CIRCLE_TAG, false)
            Log.e("AccessibilityHelpService", "无障碍服务 needAutoSign:${needAutoSign}")
            if (needAutoSign) {
                selectFlashCircle()
                when (pointsStep) {
                    1 -> traversalCircleTopTabsNodeAndSelected()
                    2 -> findSignNodeAndClick()
                    3 -> acquirePointsAndJumpNext()
                }
            } else {
                circleOpIndex = 0
                lastTabClickTime = 0L
                lastOpAddTime = 0L
                pointsStep = 1
            }
        }
    }

    /**
     * 启动闪现APP
     */
    private fun startFlashApp() {
        //获取到闪现APP的位置，然后模拟点击启动APP
        val nodeInfoList = rootInActiveWindow?.findAccessibilityNodeInfosByText(appName)
        if (nodeInfoList?.isNotEmpty() == true) {
            val itemInfo = nodeInfoList.first()
            val currentTime = System.currentTimeMillis()
            if (lastStartAppTime + startAppSpilt < currentTime) {
                lastStartAppTime = currentTime
                itemInfo.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            }
        }
    }

    private fun selectFlashTabByName(tabName: String): AccessibilityNodeInfo? {
        val nodeInfoList =
            rootInActiveWindow?.findAccessibilityNodeInfosByViewId("com.tencent.gamecommunity:id/tab_name")
        if (nodeInfoList?.isNotEmpty() == true) {
            val itemInfo = nodeInfoList.first { info -> info.text == tabName }
            return itemInfo.parent?.parent
        }
        return null
    }

    /**
     * 选中我的tab
     */
    private fun selectFlashAppMine() {
        val canClickItem = selectFlashTabByName("我的")
        if (canClickItem?.isClickable == true) {
            canClickItem.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            if (chickStep == 1) {
                chickStep = 2
            }
        } else if (canClickItem != null && !canClickItem.isClickable) {
            if (chickStep == 1) {
                chickStep = 2
            }
        }
    }

    /**
     * 选中进圈tab
     */
    private fun selectFlashCircle() {
        val canClickItem = selectFlashTabByName("进圈")
        if (canClickItem?.isClickable == true) {
            canClickItem.performAction(AccessibilityNodeInfo.ACTION_CLICK)
        }
    }

    /**
     * 遍历圈子，模拟点击
     */
    private fun traversalCircleTopTabsNodeAndSelected() {
        val topTabsNode = findCircleTopTabsNode(rootInActiveWindow)
        Log.e("AccessibilityHelpService", "无障碍服务 topTabsNode:${topTabsNode?.className}")
        if (topTabsNode != null && topTabsNode.childCount > 0) {
            val childGroup = topTabsNode.getChild(0)
            if (circleOpIndex < childGroup.childCount) {
                val clickTab = childGroup.getChild(circleOpIndex)
                val currentTime = System.currentTimeMillis()
                if (clickTab?.isClickable == true) {
                    lastTabClickTime = currentTime
                    clickTab.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                    if (pointsStep == 1) {
                        pointsStep = 2
                    }
                }
            } else {
                MMKV.defaultMMKV(MMKV.MULTI_PROCESS_MODE, null).putBoolean(MMKVKeys.CIRCLE_TAG, false)
            }
        }
    }

    /**
     * 找到打卡按钮并点击
     */
    private fun findSignNodeAndClick() {
        if (lastTabClickTime + opSpilt > System.currentTimeMillis()) {
            return
        }
        val topTabsNode = findCircleTopTabsNode(rootInActiveWindow) ?: return
        var index = 0
        val scrollNodes = findTargetNodeByClassName(rootInActiveWindow, "android.widget.ScrollView")
        if (topTabsNode.childCount > 0) {
            val childGroup = topTabsNode.getChild(0)
            if (!childGroup.getChild(circleOpIndex).isVisibleToUser) {
                return
            }
//            index = if (circleOpIndex > 0 && circleOpIndex < childGroup.childCount - 1) {
//                1
//            } else if (circleOpIndex == childGroup.childCount - 1) {
//                scrollNodes.size - 1
//            } else {
//                0
//            }
        }
        Log.e(
            "AccessibilityHelpService",
            "无障碍服务 scrollNodes:${scrollNodes.size}index:${index}circleOpIndex:${circleOpIndex}childGroup:${
                topTabsNode.getChild(
                    0
                ).childCount
            }"
        )
        if (circleOpIndex > 0 && scrollNodes.size < 2) {
            return
        }
        for (node in scrollNodes) {
            Log.e("AccessibilityHelpService", "无障碍服务 scrollNodes:${node.isVisibleToUser}")
        }
//        val scrollNode = scrollNodes.getOrNull(index)
        val scrollNode = scrollNodes.findLast { it.isVisibleToUser }
        if (scrollNode != null && scrollNode.childCount > 0) {
            val signNode = try {
                val signParentNode = scrollNode.getChild(0)?.getChild(0)?.getChild(0)?.getChild(0)?.getChild(1)
                Log.e("AccessibilityHelpService", "无障碍服务 signParentNode:${signParentNode?.childCount}")
                if (signParentNode != null && signParentNode.childCount < 3) {
                    if (pointsStep == 2) {
                        circleOpIndex++
                        pointsStep = 1
                    }
                    null
                } else if (signParentNode?.childCount == 3) {
                    signParentNode.getChild(2)
                } else {
                    null
                }
            } catch (e: Exception) {
                Log.e("AccessibilityHelpService", "无障碍服务 signNode:异常${e.message}")
                null
            }
            Log.e("AccessibilityHelpService", "无障碍服务 signNode:${signNode?.isClickable}")
            if (signNode?.isClickable == true) {
                signNode.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                if (pointsStep == 2) {
                    pointsStep = 3
                }
            }
        }
    }

    /**
     * 领取积分并且关闭弹窗，[circleOpIndex]增加
     */
    private fun acquirePointsAndJumpNext() {
        val scrollNodes = findTargetNodeByClassName(rootInActiveWindow, "android.widget.ScrollView")
        if (scrollNodes.size == 1) {
            val itemsParentNode = scrollNodes.first().getChild(0)?.getChild(0)
            if (itemsParentNode != null && itemsParentNode.childCount == 7) {
                for (index in 0 until itemsParentNode.childCount) {
                    val clickNodeParent = itemsParentNode.getChild(index)
                    if (clickNodeParent.childCount == 3) {
                        val clickNode = clickNodeParent.getChild(2)
                        if (clickNode?.isClickable == true) {
                            clickNode.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                        }
                    }
                }
                val closeNode = try {
                    scrollNodes.first().getParent().getChild(3)
                } catch (e: Exception) {
                    null
                }
                closeNode?.apply {
                    performAction(AccessibilityNodeInfo.ACTION_CLICK)
                    if (pointsStep == 3) {
                        pointsStep = 1
                        circleOpIndex++
                    }
                }
            }
        }
    }

    private fun findTargetNodeByClassName(
        findNode: AccessibilityNodeInfo?,
        className: String
    ): List<AccessibilityNodeInfo> {
        var target = mutableListOf<AccessibilityNodeInfo>()
        if (findNode != null) {
            for (index in 0 until findNode.childCount) {
                val nodeInfo = findNode.getChild(index)
                if (nodeInfo?.className == className) {
                    target.add(nodeInfo)
                }
                val temp = findTargetNodeByClassName(nodeInfo, className)
                if (!temp.isNullOrEmpty()) {
                    target.addAll(temp)
                }
            }
        }
        return target
    }

    private fun findCircleTopTabsNode(findNode: AccessibilityNodeInfo?): AccessibilityNodeInfo? {
        var target: AccessibilityNodeInfo? = null
        if (findNode != null) {
            for (index in 0 until findNode.childCount) {
                val nodeInfo = findNode.getChild(index)
                if (nodeInfo?.className == "android.widget.HorizontalScrollView") {
                    val outRect = android.graphics.Rect()
                    nodeInfo.getBoundsInScreen(outRect)
                    if (outRect.bottom < 400) {
                        target = nodeInfo
                        break
                    }
                }
                target = findCircleTopTabsNode(nodeInfo)
                if (target != null) {
                    break
                }
            }
        }
        return target
    }

    /**
     * 进入积分兑换
     */
    private fun clickPointsEvent() {
        var recycleViewNode = rootInActiveWindow?.getChild(0)
        while (true) {
            if (recycleViewNode == null) {
                break
            }
            if (recycleViewNode.className == "androidx.recyclerview.widget.RecyclerView") {
                break
            }
            recycleViewNode = recycleViewNode.getChild(0)
        }
        Log.e("AccessibilityHelpService", "无障碍服务 recycleViewNode:${recycleViewNode?.className}")
        //找到积分商城节点
        val targetNodeInfo = recycleViewNode?.getChild(1)?.getChild(0)?.getChild(0)?.getChild(3)
        if (targetNodeInfo?.isClickable == true) {
            targetNodeInfo.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            if (chickStep == 2) {
                chickStep = 3
            }
        }
    }

    private fun chickNodeClick() {
        if (rootInActiveWindow?.packageName == toPackageName) {
            val recycleNode = recycleNodeInfoFind(rootInActiveWindow)
            Log.e("AccessibilityHelpService", "无障碍服务 recycleNode:${recycleNode?.className}")
            if (recycleNode != null && recycleNode.childCount > 0) {
                val index = MMKV.defaultMMKV(MMKV.MULTI_PROCESS_MODE, null).getInt(MMKVKeys.POINTS_INDEX, 1)
                Log.e("AccessibilityHelpService", "无障碍服务 chickNodeClick:${index}")
                if (index != -1 && index < recycleNode.childCount) {
                    val node = recycleNode.getChild(index)!!
                    val clickNode = node.getChild(0)?.getChild(0)?.getChild(0)?.getChild(1)?.getChild(0)
                    if (clickNode?.isClickable == true) {
                        clickNode.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                        if (chickStep == 3) {
                            chickStep = 4
                        }
                    }
                }
            }
        }
    }

    private fun confirmExchangeChick() {
        if (rootInActiveWindow?.packageName == toPackageName) {
            val confirmNodeParent = scrollViewParentNodeFind(rootInActiveWindow)
            if (confirmNodeParent != null && confirmNodeParent.childCount > 6) {
                val clickNode = confirmNodeParent.getChild(6)
                val currentTime = System.currentTimeMillis()
                if (clickNode?.isClickable == true && chickExchangeTime + startAppSpilt < currentTime) {
                    chickExchangeTime = currentTime
                    clickNode.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                    if (chickStep == 4) {
                        chickStep = 1
                    }
                }
            }
        }
    }

    private fun scrollViewParentNodeFind(findNode: AccessibilityNodeInfo?): AccessibilityNodeInfo? {
        var target: AccessibilityNodeInfo? = null
        if (findNode != null) {
            for (index in 0 until findNode.childCount) {
                val nodeInfo = findNode.getChild(index)
                if (nodeInfo != null && nodeInfo.childCount > 5) {
                    val scrollViewNode = try {
                        nodeInfo.getChild(2)
                    } catch (e: Exception) {
                        null
                    }
                    if (scrollViewNode?.className == "android.widget.ScrollView") {
                        target = scrollViewNode.parent
                        break
                    }
                }
                target = scrollViewParentNodeFind(nodeInfo)
                if (target != null) {
                    break
                }
            }
        }
        return target
    }

    private fun recycleNodeInfoFind(findNode: AccessibilityNodeInfo?): AccessibilityNodeInfo? {
        var target: AccessibilityNodeInfo? = null
        if (findNode != null) {
            for (index in 0 until findNode.childCount) {
                val nodeInfo = findNode.getChild(index)
                if (nodeInfo != null && nodeInfo.className == "androidx.recyclerview.widget.RecyclerView") {
                    val threeLevelParent = nodeInfo.parent.parent.parent
                    if (threeLevelParent.childCount == 2) {
                        target = nodeInfo
                        break
                    }
                }
                target = recycleNodeInfoFind(nodeInfo)
                if (target != null) {
                    break
                }
            }
        }
        return target
    }

    /**
     * 每天的0点，开始抢小鸡
     */
    private fun startAction(): Boolean {
        val currentTime = System.currentTimeMillis()
//        return currentTime < TimeUtils.string2Millis("2023-08-24 23:59:59")
//                && currentTime > TimeUtils.string2Millis("2023-08-24 09:36:00")
        val endTime = Calendar.getInstance()
        endTime[Calendar.HOUR_OF_DAY] = 0
        endTime[Calendar.MINUTE] = 0
        endTime[Calendar.SECOND] = 0
        endTime[Calendar.MILLISECOND] = 0
        val endTimeInMillis = endTime.timeInMillis
        return currentTime > endTimeInMillis && currentTime - endTime.timeInMillis < spilt
    }

    override fun onInterrupt() {

    }
}