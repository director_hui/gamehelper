package com.hyh.gamehelper

import android.content.Context
import android.content.Intent
import android.provider.Settings
import androidx.lifecycle.ViewModel
import com.tencent.mmkv.MMKV

class MainViewModel : ViewModel() {

    private var mContext: Context? = null
    val toPackageName = "com.tencent.gamecommunity"

    fun init(context: Context) {
        mContext = context
    }

    fun startAction() {
        if (mContext != null) {
            if (!AccessibilityUtil.isStartAccessibilityServiceEnable(mContext)) {
                val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.resolveActivity(mContext!!.packageManager)?.let {
                    mContext!!.startActivity(intent)
                }
            }
        }
    }

    fun openFlashApp(needCheck: Boolean) {
        if (mContext != null) {
            if (!AccessibilityUtil.isStartAccessibilityServiceEnable(mContext) && needCheck) {
                val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.resolveActivity(mContext!!.packageManager)?.let {
                    mContext!!.startActivity(intent)
                }
            } else {
                val intent = mContext!!.packageManager.getLaunchIntentForPackage(toPackageName)?.apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                mContext!!.startActivity(intent)
            }
        }
    }

    fun saveFlashChickPosition(index: Int) {
        MMKV.defaultMMKV(MMKV.MULTI_PROCESS_MODE, null)
            .putInt(MMKVKeys.POINTS_INDEX, index)
        saveStartFlashCircleTag(false)
    }

    fun saveStartFlashCircleTag(open: Boolean = true) {
        MMKV.defaultMMKV(MMKV.MULTI_PROCESS_MODE, null)
            .putBoolean(MMKVKeys.CIRCLE_TAG, open)
    }
}