package com.hyh.gamehelper

object MMKVKeys {
    const val POINTS_INDEX = "points_index"
    const val CIRCLE_TAG = "circle_tag"
}